const OpenAPISnippet = require('openapi-snippet')
const yaml = require('js-yaml');
const fs = require('fs');

const openApi = yaml.load(fs.readFileSync(process.argv.slice(2)[0], 'utf8'));
const targets = ['shell_curl', 'shell_wget', 'csharp_restsharp', 'go_native', 'java_unirest', 'java_okhttp', 'php_curl', 'node_fetch', 'python_requests', 'python_python3'];

try {
    Object.keys(openApi.paths).forEach(path => {
        Object.keys(openApi.paths[path]).forEach(method => {
            const snippets = OpenAPISnippet.getEndpointSnippets(openApi, path, method, targets);
            const samples = []
            openApi.paths[path][method]['x-code-samples'] = samples;
            snippets.snippets.forEach(snippet => {
                samples.push({
                    lang: snippet.title.split(' ')[0],
                    source: snippet.content
                })
            })
        })
    })
    console.log(yaml.dump(openApi))
} catch (err) {
    console.log(err)
}